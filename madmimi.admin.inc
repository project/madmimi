<?php

/**
 * @file
 * Administrative page callbacks for the madmimi module.
 */

/**
 * Define the settings form.
 */
function madmimi_admin_settings() {
  $form = array();
  $form['madmimi_username'] = array(
    '#title' => 'Mad Mimi Username',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('madmimi_username', NULL),
    '#size' => 60,
    '#maxlength' => 128
  );
  
  $form['madmimi_apikey'] = array(
    '#title' => 'Mad Mimi API Key',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('madmimi_apikey', NULL),
    '#size' => 60,
    '#maxlength' => 128,
    '#description' => t('Be sure your account has the Mailer API feature, available <a href="http://madmimi.com/addons">here</a>.'),
  );
  
  $form['madmimi_promotionname'] = array(
    '#title' => 'Promotion Name',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('madmimi_promotionname', NULL),
    '#size' => 60,
    '#maxlength' => 128
  );
  
  return system_settings_form($form);  
}